/*
 * file      : mysql/mysql_version.h
 * copyright : Copyright (c) 2016-2019 Code Synthesis Ltd
 * license   : GPLv2 with FOSS License Exception; see accompanying COPYING file
 */

#include <mysql/version.h>

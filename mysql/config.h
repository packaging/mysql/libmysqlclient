/* file      : mysql/config.h -*- C -*-
 * copyright : Copyright (c) 2016-2019 Code Synthesis Ltd
 * license   : GPLv2 with FOSS License Exception; see accompanying COPYING file
 */

/*
 * The upstream package auto-generated my_config.h and config.h (used by the
 * bundled libs) are identical, so we just express one through the other.
 */
#include <mysql/my_config.h>
